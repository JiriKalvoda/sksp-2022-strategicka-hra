# Server na strategicou hru na SKSP2022

Velké poděkování patří Medvědovi a Jirkovi Setničkovi, jejichž kód vykrádám.

## Instalace vývojového prostředí

	python3 -m venv venv
	. venv/bin/activate
	pip install wheel
	pip install -c constraints.txt --editable .
	# vytvořit hra/config.py podle config.py.example

	# Jako admin postgresu
	psql
		postgres=# CREATE DATABASE ksp_strathra_devel WITH OWNER=$(UŽVATELSKÉ JMÉNO);
	
	# Naplníme databázi
	bin/db_init [--drop]

	# Přístup do orgočásti webu
	bin/create_user --org root <heslo>

	#
	bin/build nebo bin/update

	./flask run

	# Po běžných úpravách není potřeba balíček přeinstalovávat

## CMD ovládání

Některé části serveru se nedají ovládat přes web, je nutné pouštět skripty v adresáři
`bin`. Před spouštěním je nutné aktivovat venv!

Vytváření nové hry (konfigurace přepsáním programu):
	./bin/create_game

Každý uživatel při vzniku dostane také testovací hru (parametry v `hra/lib.py`).

Ovládání hry:
	./bin/control_game

Aby fungovali automatické kroky, je nutné nechat běžet
	./bin/control_game [id] --autosteps

Vytváření (orgovského) účtu:
	./bin/create_user [--org]

## Instalace na produkční server

	# Pro systém s jádrem < 5.4 zvýšit net.core.somaxconn (jako root)
	[ "`cat /proc/sys/net/core/somaxconn`" -lt 4096 ] && echo net.core.somaxconn=4096 >> /etc/sysctl.conf && sysctl -p

	# Založit účet (jako root)
	useradd -m -s /usr/bin/bash ksp-strathra-web

	# Založit databázi (jako správce PostgreSQL)
	psql -e -c 'CREATE ROLE ksp_strathra'
	psql -e -c 'CREATE ROLE "ksp-strathra-web" LOGIN'
	psql -e -c 'GRANT ksp_strathra TO "ksp-strathra-web"'	# a případně dalším uživatelům
	psql -e -c 'CREATE DATABASE ksp_strathra_pub WITH OWNER=ksp_strathra'

	# Nastavit uživateli SSHčko (jako on)
	mkdir .ssh
	vi authorized_keys

	# Pushnout data (jako svůj uživatel v složce s README)
	bin/build nebo bin/update
	bin/deploy

	# Nainstalovat venv
	python3 -m venv venv
	. venv/bin/activate
	pip install wheel
	pip install -c constraints.txt --editable .

	# Inicializovat databázi (už jako mo-web)
	bin/db_init [--drop]

	# Pustit web
	uwsgi --http 127.0.0.1:8000 --master -p 4 -w  hra.web:app

## Mražení závislostí

	pip freeze | grep -v '^hra=' >constraints.txt
