#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name='hra',
    version='1.0',
    description='Hra na soustředění KSP',
    packages=['hra', 'hra/web'],
    scripts=[
        "bin/db_init",
        "bin/create_user",
        "bin/create_game",
        "bin/control_game",
    ],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # Udržujte prosím seřazené
        'Flask',
        'Flask-WTF',
        'WTForms',
        'bcrypt',
        'bleach',
        'blinker',
        'click',
        'dateutils',
        'flask_bootstrap',
        'flask_sqlalchemy',
        'numpy',
        'pikepdf',
        'pillow',
        'psycopg2',
        'pyzbar',
        'sqlalchemy[mypy]',
        'uwsgi',
        'uwsgidecorators',
        # Používáme pro vývoj, ale aby je pylsp našel, musí být ve stejném virtualenvu
        # jako ostatní knihovny.
        'sqlalchemy-stubs',
        'types-Markdown',
        'types-bleach',
        'types-flask_sqlalchemy',
        'types-pillow',
        'types-python-dateutil',
        'types-requests',
        'types-setuptools',
    ],
)
