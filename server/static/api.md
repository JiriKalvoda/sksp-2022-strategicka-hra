Obsazování území
================

API
---

Odpověď na libovolný dotaz muže být HTTP error ve tvaru:
```
{
	status: "error"
	description: <str> # Popis chybové události (toto chceme vytisknout!)
	http-code: <int>
	http-name: <str> # Jméno chybového kódu
}
```

Parametry:
- token: Identifikuje uživatele, zjistí z hlavní stránky webu
- game: S jakou hrou (popř. týmem ve hře) se chtějí bavit
	- Pro naše potřeby:
		- `main` - Hlavní hra
		- `test_{0,1,..}` Testovací hra daného uživatele (číslo je jako který tým hrají)
- round_id: To zjistí z otazu na stav
- min_round: Validní odpovědi pouze pokud je tento nebo pozdější tah, jinak "too_early"

Stav:
GET na `/api/state?game=<jmeno_hry>&token=<token>&min_round=<min_round>`
```json
{
	status: "ok"
	round: <int> # Kolikáté je aktuální kolo (na počátku 0)
	team_id: <int>
	teams_count: <int>
	time_to_response: <Optional[int]> # None v případě, že ho neznáme
	state:
	{
		world: [[{
			home_for_team: <Optional[int]>
			occupied_by_team: <Optional[int]>
			protected_for_team: <Optional[int]>
			hill: <bool>
			members: [{
				type: "soldier" # Pro budoucí rozšiřování
				team: <int>
				id: <int> # Unikátní číslo v rámci týmu
				remaining_rounds: <int> # vždy > 0
			}]
		}]]
	}
}

nebo

{
	status: "working" nebo "too_early" # Server počítá následující stav nebo je některý z předchozích tahů, klient má počkat
	wait: <float> # Jak dlouho má klient čekat, než se zeptá znovu
}
```

Je zaručeno, že domeček mají uprostřed mapy


Tah:
POST na `/api/action?game=<jmeno_hry>&token=<token>&round=<round_id>`
```json
{
	members: [
		id: <int>
		action: <Optional[str]>
			# None -> čekej
			# "left", "right", "up", "down"
	]
}
```

Odpověď na tah:
```
{
	status: "ok"
} # Vše v pořádku

nebo

{
	status: "warning"
	members: [ # seznam osob s chybou/varováním
		id: <int>
		description: <str>
	]
} # Tah jako celek se provede, chyba jen u některých osob

nebo

{
	status: "error"
	description: <str>
} # Chyba celého tahu (např. nevalidní json, obsahuje povinné hodnoty ...)

nebo

{
	status: "too-late"
}
```

Požadavek na krok:
POST na `/api/step?game=<jmeno_hry>&token=<token>`

Odpověď:
```
{
	status: "ok",
}

nebo

{
	status: "too_early"
	wait: 5
}
```
