import bcrypt
from time import time

def hash_passwd(a):
    salt = b'$2b$12$V2aIKSJC/uozaodwYnQX3e'
    hashed = bcrypt.hashpw(a.encode('utf-8'), salt)
    return hashed.decode('us-ascii')

def timer_func(func):
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'Function {func.__name__!r} executed in {(t2-t1):.4f}s')
        return result
    return wrap_func
