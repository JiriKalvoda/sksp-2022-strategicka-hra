from hra.web import app

quick_form = app.jinja_env.get_template("bootstrap/wtf.html").module.quick_form
form_field = app.jinja_env.get_template("bootstrap/wtf.html").module.form_field
