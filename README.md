Strategická hra na soustředění KSP 2022.
========================================

    Copyright (C) 2022 Jiří Kalvoda <jirikalvoda@kam.mff.cuni.cz>
    Copyright (C) 2022 David Klement
    Copyright (C) 2022 Martin Koreček

    Some parts of code is from OSMO:

    Copyright (C) 2020 - 2022 Martin Mareš
    Copyright (C) 2020 - 2022 Jiří Setnička
    Copyright (C) 2021 - 2022 Jiří Kalvoda

    And MHD game on SKSP2021:

    Copyright (C) 2022 Jiří Kalvoda <jirikalvoda@kam.mff.cuni.cz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Koncept hry
===========

Jedná se o programovací hru postavenou okolo web serveru a JSON API.

Hra se hraje v tazích po 10 až 60 sekundách. V každém tahu si každý soutěžící stáhne
stav hry a odpoví svým tahem. Ten se pak centrálně vyhodnotí.

Předpokládá se, že hráči upravují svůj program v závislosti na aktuálním stavu hry.


Instalace
=========

Viz `server/README.md`

Hry
===

Momentálně je implementována jediná hra -- Obsazování území.
Viz `pravidla.md`.

Web server do ovšem psaný dostatečně obecně na to aby šli další hry přidat.
Podívejte se do `server/hra/game.py` a případně do `server/hra/web/game.py`,
kde je zobrazování stavu hry jako webové stránky.
