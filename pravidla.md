\pagenumbering{gobble}

# Dobyvatelé

Hra probíhá v taktech, v každém taktu máte možnost dát povel každému svému vojákovi pomocí
jeho unikátního ID, kam se má
vydat (dá se pohybovat doleva, doprava, nahoru, dolů, a dá se také zůstat na místě do dalšího taktu).
Na každém poli smí být, a nejspíš často bude, několik vojáků vašeho týmu. Některá pole plánku
jsou od začátku zabrána kopci (v API jim říkáme `hill`), kam vojáci nedokážou jít. Pokud se o to pokusí,
zůstanou na aktuálním poli, jako by se o kopec zarazili.

Pokud pohyby vojáků vedou během taktu na
střet vojáků různých týmů na jednom políčku, vojáci se pomlátí způsobem popsaným níže. Přežít ale 
mohou vojáci maximálně jednoho týmu na poli, a to pouze pokud zde tým má ostře nejvíc vojáků.

## Cíl hry

Na konci taktu, kdy každé pole okupuje maximálně jeden tým, se pole okupované každým týmem stane
územím tohoto týmu, a to až do chvíle, kdy je zabere tým jiný.

Po **každém** taktu se spočte, kolik území náleží každému týmu, a toto číslo se přičte ke množství *bodů*
týmu. To znamená, že finální bodování je *součtem* týmem zabraných polí *po každém taktu*.

## Jak do hry vstupují noví vojáci

Před začátkem každého taktu mají všechny týmy stejný *pomyslný rozpočet*. Z toho musí nejprve uživit všechny své živé
vojáky (to vždy půjde), a poté mohou nakoupit nové. Uživení každého stávajícího vojáka je stejně nákladné
a naverbování nového stojí o poznání víc.

Naverbovaní vojáci se objeví vždy ve vašem domečku (domeček je jedno speciální pole).
Čtverec 3x3 polí s domečkem ve středu je chráněný a jiné týmy se do něj nemohou dostat,
podobně jako kdyby šlo o kopec.

Rozpočet v průběhu hry lineárně stoupá (vy ale explicitně rozpočet neuvidíte, jen vám podle této logiky přibudou vojáci).

## Souboje dvou vojsk

Když se v jednom poli střetne více týmů, přežijí zde jen vojáci týmu s ostře největším počtem vojáků na tomto poli.
Pokud takový tým není, nepřežije zde žádný voják a pole bude dál okupované stejným týmem jako před taktem
(nebo volné).

Pokud označíme jako $a$ počet vojáků nejsilnějšího týmu v poli a $b$ počet toho druhého nejsilnějšího,
ubyde nejsilnějšímu týmu na konci taktu na daném poli (dolní celá část z) $\frac{b^2}{a}$ vojáků.
