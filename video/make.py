#!/usr/bin/python3
# importing webdriver from selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from PIL import Image
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--token", help="Token na účet s přístupem")

args = parser.parse_args()

 
# Here Chrome  will be used
chrome_options = Options()
chrome_options.add_argument("--window-size=1920,1080")
chrome_options.add_argument("headless")
# chrome_options.add_argument("--no-startup-window")
driver = webdriver.Chrome(chrome_options=chrome_options)
 
# Opening the website

for i in range(333):
    url = f"http://localhost:8000/game/4/view?font_size=9&refresh=Aktualizace+pomoc%C3%AD+HTML+META&clickable=Vypnout+podrobnosti+kliknut%C3%ADm+(zmen%C5%A1%C3%AD+n%C3%A1roky+na+s%C3%ADt+a+procesor)&status=Metadata+vlevo+od+mapy&show_data_only=Zobrazit+str%C3%A1nku+pouze+s+daty&round={i}&token={args.token}"
    print(url)
    driver.get(url)
    time.sleep(0.1)
    driver.save_screenshot(f"image_{i}.png")
    print(f"DONE {i}")

subprocess.run("ffmpeg -framerate 10 -i 'image_%d.png' -c:a aac  v.mp4", shell=True, check=True)

